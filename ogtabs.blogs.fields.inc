<?php
/**
 * @file
 * All the blogs content type extra fields which are added via .install
 */

/**
 * Define the fields for our content type.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the fields we wish to add to our
 * new node type.
 */
function _blogs_installed_fields() {
  return array(
    'field_blog_visibility' => array(
      'field_name' => 'field_blog_visibility',
      'cardinality' => 1,
      'entity_type' => 'node',
      'type' => 'list_text',
      'label' => t('Blog Visibility'),
      'description' => t('Choose the visibility options for your blog.'),
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'settings' => array(
        'allowed_values' => array(
          'instructor' => 'Instructor',
          'whole_class' => 'Whole class',
          'unpublished' => 'Leave unpublished',
        ),
      ),
      'default_value' => array(array('value' => 'instructor')),
    ),
  );
}

/**
 * Define the field instances for our content type.
 *
 * The instance lets Drupal know which widget to use to allow the user to enter
 * data and how to react in different view modes.  We are going to display a
 * page that uses a custom "node_example_list" view mode.  We will set a
 * cardinality of three allowing our content type to give the user three color
 * fields.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the instances we wish to add to our new
 * node type.
 */
function _blogs_installed_instances() {
  return array(
    'field_blog_visibility' => array(
      'field_name' => 'field_blog_visibility',
      'cardinality' => 1,
      'entity_type' => 'node',
      'type' => 'list_text',
      'label' => t('Blog Visibility'),
      'description' => t('Choose the visibility options for your blog.'),
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'settings' => array(
        'allowed_values' => array(
          'instructor' => 'Instructor',
          'whole_class' => 'Whole class',
          'unpublished' => 'Leave unpublished',
        ),
      ),
      'default_value' => array(array('value' => 'instructor')),
    ),
  );
}
