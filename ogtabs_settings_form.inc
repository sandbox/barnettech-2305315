<?php

/**
 * @file
 * Google Drive api connection
 * Settings form for ID and Secrets
 */
/**
 * Uses the form api for an admin settings form.
 */
function ogtabs_settings_form() {
    global $user;
    $ogtabs_announcements = variable_get('ogtabs_announcements', '');
    $ogtabs_blogs = variable_get('ogtabs_blogs', '');
    $ogtabs_wikis = variable_get('ogtabs_wikis', '');
    $ogtabs_polls = variable_get('ogtabs_polls', '');
    $ogtabs_events = variable_get('ogtabs_events', '');
    $ogtabs_syllabi = variable_get('ogtabs_syllabi', '');

    $options = array(
        'announcement' => t('announcements'),
        'blogs' => t('blogs (this blog type is ideal for schools / teaching environments)'),
        'blogs2' => t('blogs (plain old regular blogs)'),
        'wiki' => t('wikis'),
        'poll' => t('polls'),
        'events' => t('events'),
        'syllabi' => t('syllabi')
    );
    $default_value = variable_get('ogtabs_publishable', '');
    $form['container']['ogtabs_publishable'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Allow users to post:'),
        '#options' => $options,
        '#default_value' => $default_value,
    );

    $form = system_settings_form($form);
    $form['#submit'][] = 'ogtabs_settings_form_submit';
    return $form;
}

/**
 * Form submission handler ogtabs_settings_form().
 */
function ogtabs_settings_form_submit($form, &$form_state) {

}