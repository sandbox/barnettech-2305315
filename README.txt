Welcome to the OGTabs module, installation is fairly easy:

1.)  Enable the ogtabs module
2.)  Go and make sure a group content type exists called "group" (or whatever name really should work) and within this content type make sure it's set to be a "Group" (an organic group).
3.)  Make sure all the other other content types that are going to be pieces of content in the group are set as "Group content" and then under "target bundles" choose the group content type they belong to.  This is all done within the admin/structure/types area where you assign content types to groups and create base group content types.
4.)  Enable the "OGTABS Create Selective Types of Group Content" block and put it on the pages you would like it to appear on, I suggest just having it on the organic group home pages. (use the Drupal blocks system to do this)
5.)  If using syllabi for administrators only enable the "Syllabus Draggable Edit Form" block only on the node/*/syllabi_list pages within the Drupal blocks system.
