<?php
/**
 * @file
 * All the events content type extra fields which are added via .install
 */

/**
 * Define the fields for our content type.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the fields we wish to add to our
 * new node type.
 */
function _events_installed_fields() {
  return array(
    'field_events_start_date' => array(
      'field_name'   => 'field_events_start_date',
      'label'        => t('Start and End Dates'),
      'cardinality'  => 1,
      'type'         => 'datetime',
      'settings' => array(
        'todate' => 'optional',
      ),
    ),
    'field_events_attachments' => array(
      'field_name' => 'field_events_attachments',
      'type' => 'file',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'entity_type' => 'node',
    ),
  );
}

/**
 * Define the field instances for our content type.
 *
 * The instance lets Drupal know which widget to use to allow the user to enter
 * data and how to react in different view modes.  We are going to display a
 * page that uses a custom "node_example_list" view mode.  We will set a
 * cardinality of three allowing our content type to give the user three color
 * fields.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the instances we wish to add to our new
 * node type.
 */
function _events_installed_instances() {
  return array(
    'field_events_start_date' => array(
      'field_name'  => 'field_events_start_date',
      'label'       => t('Start and End Dates'),
      'widget' => array(
        'active' => 1,
        'module' => 'date',
        'settings' => array(
          'increment' => 15,
          'input_format' => 'm/d/Y - H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
          'todate' => 'optional',
        ),
        'type' => 'date_select',
        'weight' => '3',
      ),
    ),
    'field_events_attachments' => array(
      'field_name' => 'field_events_attachments',
      'entity_type' => 'node',
      'label' => t('File attachments'),
      'bundle' => 'forum',
      'settings' => array(
        'file_extensions' => 'txt pdf, doc docx ppt pptx xls xlsx jpg png gif',
      ),
    ),
  );
}
