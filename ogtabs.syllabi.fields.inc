<?php
/**
 * @file
 * All the syllabi content type extra fields which are added via .install
 */

/**
 * Define the fields for our content type.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the fields we wish to add to our
 * new node type.
 */
function _syllabi_installed_fields() {
  return array(
    'field_syllabi_start_date' => array(
      'field_name'   => 'field_syllabi_start_date',
      'label'        => t('Start and End Dates'),
      'cardinality'  => 1,
      'type'         => 'datetime',
      'settings' => array(
        'todate' => 'optional',
      ),
    ),
    'field_syllabi_attachments' => array(
      'field_name' => 'field_syllabi_attachments',
      'type' => 'file',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'entity_type' => 'node',
    ),
    'field_syllabi_weight' => array(
      'field_name'  => 'field_syllabi_weight',
      'label'       => t('Syllabi Weight'),
      'type'       => 'number_integer',
      'description'  => t('The weight field for the syllabi content type.'),
      'entity_type' => 'node',
      'cardinality' => 1,
    ),
    'field_syllabi_pid' => array(
      'field_name'  => 'field_syllabi_pid',
      'label'       => t('Syllabi PID'),
      'type'       => 'number_integer',
      'description'  => t('The PID field for the syllabi content type.'),
      'entity_type' => 'node',
      'cardinality' => 1,
    ),
    'field_syllabi_depth' => array(
      'field_name'  => 'field_syllabi_depth',
      'label'       => t('Syllabi Depth'),
      'type'       => 'number_integer',
      'description'  => t('The depth field for the syllabi content type.'),
      'entity_type' => 'node',
      'cardinality' => 1,
    ),
  );
}

/**
 * Define the field instances for our content type.
 *
 * The instance lets Drupal know which widget to use to allow the user to enter
 * data and how to react in different view modes.  We are going to display a
 * page that uses a custom "node_example_list" view mode.  We will set a
 * cardinality of three allowing our content type to give the user three color
 * fields.
 *
 * This big array is factored into this function for readability.
 *
 * An associative array specifying the instances we wish to add to our new
 * node type.
 */
function _syllabi_installed_instances() {
  return array(
    'field_syllabi_start_date' => array(
      'field_name'  => 'field_syllabi_start_date',
      'label'       => t('Start and End Dates'),
      'widget' => array(
        'active' => 1,
        'module' => 'date',
        'settings' => array(
          'increment' => 15,
          'input_format' => 'm/d/Y - H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
          'todate' => 'optional',
        ),
        'type' => 'date_select',
        'weight' => '3',
      ),
    ),
    'field_syllabi_attachments' => array(
      'field_name' => 'field_syllabi_attachments',
      'entity_type' => 'node',
      'label' => t('File attachments'),
      'bundle' => 'forum',
      'settings' => array(
        'file_extensions' => 'txt pdf, doc docx ppt pptx xls xlsx jpg png gif',
      ),
    ),
    'field_syllabi_weight' => array(
      'field_name'   => 'field_syllabi_weight',
      'label'        => t('Syllabi Weight'),
      'description'  => t('The weight field for the syllabi content type.'),
      'cardinality'  => 1,
      'type'         => 'number_integer',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'hidden',
        ),
      ),
      'default_value' => array(array('value' => '0')),
    ),
    'field_syllabi_pid' => array(
      'field_name'   => 'field_syllabi_pid',
      'label'        => t('Syllabi PID'),
      'description'  => t('The PID field for the syllabi content type.'),
      'cardinality'  => 1,
      'type'         => 'number_integer',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'hidden',
        ),
      ),
      'default_value' => array(array('value' => '0')),
    ),
    'field_syllabi_depth' => array(
      'field_name'   => 'field_syllabi_depth',
      'label'        => t('Syllabi Depth'),
      'description'  => t('The depth field for the syllabi content type.'),
      'cardinality'  => 1,
      'type'         => 'number_integer',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'hidden',
        ),
      ),
      'default_value' => array(array('value' => '0')),
    ),
  );
}
