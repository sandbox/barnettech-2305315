<?php
/**
 * @file
 * Adds content integrated into organic groups such as blogs, syllabi wikis.
 */

/**
 * Implements hook_help().
 */
function ogtabs_help($path, $arg) {
  switch ($path) {
    case 'admin/help#ogtabs':
      return '<p>' . t('Adds content integrated into organic groups such as blogs, syllabi wikis.') . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function ogtabs_permission() {
    return array(
        'administer ogtabs' => array(
            'title' => t('administer ogtabs'),
            'description' => t('administer ogtabs'),
        ),
    );
}

/**
 * Implements hook_menu().
 */
function ogtabs_menu() {
  $items['admin/config/ogtabs/settings'] = array(
    'title' => 'OGTABS Settings',
    'description' => 'Configuration of the OGTABS module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ogtabs_settings_form'),
    'access arguments' => array('administer ogtabs'),
    'file' => 'ogtabs_settings_form.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['examples/ogtabs_parent'] = array(
    'title' => 'TableDrag example (parent/child)',
    'description' => 'Show a page with a sortable parent/child tabledrag form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ogtabs_parent_form'),
    'access callback' => TRUE,
    'file' => 'ogtabs_parent_form.inc',
  );
  $items['node/%/syllabi_list'] = array(
    'title' => 'Syllabus',
    'page callback' => 'ogtabs_syllabi_list',
    'access callback' => 'ogtabs_extras_access_syllabi',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'ogtabs.pages.inc',
  );
  $items['node/%/calendar-page'] = array(
    'title' => 'Calendar',
    'page callback' => 'ogtabs_calendar_page',
    'access callback' => 'ogtabs_extras_access_calendar',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'ogtabs.pages.inc',
  );
  $tab_types = array('blogs', 'blogs2', 'wiki', 'poll', 'events');
  foreach ($tab_types as $tab) {
    if ($tab == 'blogs2') {
      $tab_title = 'blogs';
    }
    else {
      $tab_title = $tab;
    }
    $items['node/%/' . $tab . '-list'] = array(
      'title' => ucfirst($tab_title),
      'page callback' => 'ogtabs_tab_' . $tab . '_list',
      'access callback' => 'ogtabs_tabs_extras_access_content',
      'access arguments' => array(1, $tab),
      'type' => MENU_LOCAL_TASK,
      'file' => 'ogtabs.pages.inc',
    );
    $tab_name_singular = rtrim($tab, "s");
    $items['node/%node/' . $tab . '-list/' . $tab_name_singular . '/%node'] = array(
      'title callback' => 'node_page_title',
      'title arguments' => array(4),
      'page callback' => 'ogtabs_type_wrapper',
      'access callback' => 'sts_commons_forum_page_topic_access',
      'access arguments' => array(3),
      'page arguments' => array(1, 4),
      'access callback' => TRUE,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
  }
  return $items;
}

function ogtabs_contains($str, array $arr)
{
  foreach($arr as $a) {
    if (stripos($str,$a) !== false) return true;
    $str = $str . '/';
    if (stripos($str,$a) !== false) return true;
  }
  return false;
}

/**
 * Implements hook_node_view().
 */
function ogtabs_node_view($node, $view_mode, $langcode) {
  $current_path = current_path();
  $types = array('syllabi', 'blogs', 'blogs2', 'wikis', 'poll', 'events');
  $paths = array("/edit/", "/syllabus/", "/syllabi_list/", "/wikis-list/", "/blogs-list/", "/blogs2-list/", "/poll-list/", "/revisions/", "/events-list/", "/reply/");
  if ($view_mode == 'search_result' || $view_mode == 'search_index' || !in_array($node->type, $types) || ogtabs_contains($current_path, $paths)) {
    return;
  }
  $path = ogtabs_content_preferred_path($node);
  if (!drupal_is_cli()) {
    drupal_goto($path);
  }
}

/**
 * Implements hook_node_update().
 */
function ogtabs_node_update($node) {
    watchdog('ogtabs', 'type is ..... ' . $node->type);
    if ($node->type == 'wiki' || $node->type == 'blogs' || $node->type == 'blogs2' || $node->type == 'poll' || $node->type == 'events') {
        $path = ogtabs_content_preferred_path($node);
        if (!drupal_is_cli()) {
            drupal_goto($path);
        }
    }
}

/**
 * Implements hook_node_update().
 */
function ogtabs_node_insert($node) {
    watchdog('ogtabs', 'type is ..... ' . $node->type);
    if ($node->type == 'wiki' || $node->type == 'blogs' || $node->type == 'blogs2' || $node->type == 'poll' || $node->type == 'events') {
        $path = ogtabs_content_preferred_path($node);
        if (!drupal_is_cli()) {
            drupal_goto($path);
        }
    }
}

/**
 * Page callback: render $forum as a tab on the $course page.
 *
 * @param Object $course
 *   the node object for the course
 * @param Object $node
 *   the node object for the forum
 *
 * TODO:
 *   - (not here) set the URL alias.
 */
function ogtabs_type_wrapper($course, $node) {
  global $user;
  $view = node_view($node, 'full');
  $rendered = drupal_render($view);
  $edit_link = l(t('edit'), 'node/' . $node->nid . '/edit');
  $gid = arg(1);
  $content = '';
  if ($node->uid == $user->uid  ||  ogtabs_check_if_group_admin_or_ta($gid)) {
    $content .= '<div class="ogtabs-edit-button" >' . $edit_link . '</div><div></div>';
  }
  $formatted_date = format_date($node->created, 'medium', 'F j, Y, g:i a');
  return $content . ' ' . $rendered . 'Created on:&nbsp;&nbsp;' . $formatted_date;
}


/**
 * Generate the preferred path for viewing a forum, as a tab on its group page.
 *
 * @param Object $node
 *   the node object for the forum to be viewed
 * @param Boolean $add_title
 *   (optional) if TRUE (the default) then add sanitized forum title to path.
 *
 * @return String
 *   The preferred path or empty if $forum is not part of a group.
 */
function ogtabs_content_preferred_path($node, $add_title = TRUE) {
  // In case the caller passed the nid instead of the node object.
  watchdog('ogtabs', 'doing the redirect <pre>' . print_r($node, TRUE) . '</pre>');
  if (isset($node->og_group_ref['und'][0]['target_id'])) {
    $gid = $node->og_group_ref['und'][0]['target_id'];
  }
  if (isset($gid) && $node->type == 'syllabi') {
    $path = 'http://' . $_SERVER['HTTP_HOST'] . "/node/$gid/syllabi_list";
  }
  else {
    $tab_name_singular = rtrim($node->type, "s");
    $path = 'http://' . $_SERVER['HTTP_HOST'] . '/node/' . $gid . '/' . $node->type . '-list/' . $tab_name_singular . '/' . $node->nid;
  }
  return $path;
}

/**
 * Implements hook_node_view_alter().
 */
function ogtabs_node_view_alter(&$build) {
  $node = $build['#node'];
  if ($build['#view_mode'] == "full" && ($node->type == "poll" || $node->type == "syllabi")) {
    $breadcrumb = array();
    $breadcrumb = drupal_get_breadcrumb();
    drupal_set_breadcrumb($breadcrumb);
  }
}

/**
 * Implements hook_theme().
 *
 * We need to run our forms through custom theme functions in order to build the
 * table structure which is required by tabledrag.js.  Before we can use our
 * custom theme functions, we need to implement hook_theme in order to register
 * them with Drupal.
 *
 * We are defining our theme hooks with the same name as the form generation
 * function so that Drupal automatically calls our theming function when the
 * form is displayed.
 */
function ogtabs_theme() {
  return array(
    // Theme function for the 'parent/child' example.
    'ogtabs_parent_form' => array(
      'render element' => 'form',
      'file' => 'ogtabs_parent_form.inc',
    ),
    'ogtabs_syllabi_list' => array(
      'arguments' => array(
        'service' => NULL,
        'owner_email' => NULL,
      ),
      'file' => 'ogtabs.pages.inc',
    ),
    'ogtabs_tab_blogs_list' => array(
      'arguments' => array(
      'service' => NULL,
       'owner_email' => NULL,
    ),
    'file' => 'ogtabs.pages.inc',
    ),
    'ogtabs_tab_blogs2_list' => array(
        'arguments' => array(
        'service' => NULL,
        'owner_email' => NULL,
      ),
      'file' => 'ogtabs.pages.inc',
    ),
    'ogtabs_tab_events_list' => array(
      'arguments' => array(
        'service' => NULL,
        'owner_email' => NULL,
      ),
      'file' => 'ogtabs.pages.inc',
    ),
    'ogtabs_tab_wiki_list' => array(
      'arguments' => array(
        'service' => NULL,
        'owner_email' => NULL,
      ),
      'file' => 'ogtabs.pages.inc',
    ),
    'ogtabs_tab_poll_list' => array(
      'arguments' => array(
        'service' => NULL,
        'owner_email' => NULL,
      ),
      'file' => 'ogtabs.pages.inc',
    ),
    'node__syllabi' => array (
            'variables' => array(),
            'template' => 'node-syllabi' ,
            'base hook' => 'node',
            'path' => drupal_get_path('module', 'ogtabs'),
        ),
  );
}

/**
 * Implements hook_block_info().
 */
function ogtabs_block_info() {
  $blocks = array();
  $blocks['ogtabs_draggable_form'] = array(
    'info' => t('Syllabus Draggable Edit Form'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['create-any-content'] = array(
    'info' => t('OGTABS Create Any Type of Group Content'),
  );
  $blocks['create-selective-content'] = array(
    'info' => t('OGTABS Create Selective Types of Group Content'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function ogtabs_block_view($delta) {
  $blocks = array();
  switch ($delta) {
    case 'ogtabs_draggable_form':
      require_once 'ogtabs_parent_form.inc';
      $blocks['subject'] = t('Syllabus Draggable Sort Order Form');
      $blocks['content'] = drupal_get_form('ogtabs_parent_form');
      break;

    case 'create-any-content':
      $content = drupal_get_form('ogtabs_create_content_form');
      if (module_exists('og_context')) {
        $current_group = og_context();
      }
      $gid = $current_group['gid'];
      $is_admin_or_ta = ogtabs_check_if_group_admin_or_ta($gid);
      $group_type = db_query('SELECT nt.name from {node} n, {node_type} nt where nt.type = n.type and n.nid = :nid', array('nid' => $gid))->fetchField();
      $blocks['subject'] = t('Create Content');
      $students_can_publish = variable_get('ogtabs_non_group_admin_can_publish_' . $gid, '');
      $show_block = FALSE;
      $students_can_publish = explode(",", $students_can_publish);
      if (strlen($students_can_publish[0]) == 0) {
          $show_block = FALSE;
      }
      else {
          $show_block = TRUE;
      }
      if ($is_admin_or_ta) {
        $blocks['content'] = $content;
      }
      elseif ($show_block) {
        $blocks['content'] = $content;
      }
      else {
        return FALSE;
      }
      break;
    case 'create-selective-content':
      $content = drupal_get_form('ogtabs_create_content_form');
      if (module_exists('og_context')) {
        $current_group = og_context();
      }
      if (!isset($current_group['gid'])) {
        $curr_url = current_path();
        $path_array = explode('/', $curr_url);
        $gid = $path_array[1];
      }
      else {
        $gid = $current_group['gid'];
      }
      $is_admin_or_ta = ogtabs_check_if_group_admin_or_ta($gid);
      $group_type = db_query('SELECT nt.name from {node} n, {node_type} nt where nt.type = n.type and n.nid = :nid', array('nid' => $gid))->fetchField();
      $blocks['subject'] = t('Create @type Content', array('@type' => $group_type));
      $students_can_publish = variable_get('ogtabs_non_group_admin_can_publish_' . $gid, '');
      $show_block = FALSE;
      $students_can_publish = explode(",", $students_can_publish);
      if (strlen($students_can_publish[0]) == 0) {
          $show_block = FALSE;
      }
      else {
          $show_block = TRUE;
      }
      if ($is_admin_or_ta) {
        $blocks['content'] = $content;
      }
      elseif ($show_block) {
        $blocks['content'] = $content;
      }
      else {
        return FALSE;
      }
      break;
  }
  return $blocks;
}

/**
 * Get Syllabi access count.
 */
function ogtabs_extras_access_syllabi($gid) {
  $syllabi_count = db_query("SELECT count(*) as count from {node} n, {og_membership} om where om.etid = n.nid
    and n.type = 'syllabi' and om.gid = :gid", array('gid' => $gid))->fetchField();
  if ($syllabi_count < 1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Get Syllabi and Events access count.
 */
function ogtabs_extras_access_calendar($gid) {
  $syllabi_count = db_query("SELECT count(*) as count from {node} n, {og_membership} om where om.etid = n.nid
    and n.type in ('syllabi', 'events') and om.gid = :gid", array('gid' => $gid))->fetchField();
  if ($syllabi_count < 1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}


/**
 * Get content access count.
 */
function ogtabs_tabs_extras_access_content($gid, $type) {
  $type_count = db_query("SELECT count(*) as count from {node} n, {og_membership} om where om.etid = n.nid
    and n.type = :type and om.gid = :gid", array('gid' => $gid, 'type' => $type))->fetchField();
  if ($type_count < 1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_views_api().
 */
function ogtabs_views_api() {
  return array(
    'api' => 3,
  );
}

function ogtabs_blog_form($form, &$form_state) {
  $form['author'] = array(
    '#type' => 'textfield',
    '#parents' => array('author'),
    '#title' => t('Authored by'),
    '#size' => 30,
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => '',
    '#weight' => -1
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function ogtabs_blog_form_submit ($form, &$form_state) {
  $_SESSION['blog_filter'] = $form_state['values']['author'];
}



/**
 * Implements hook_form_alter().
 */
function ogtabs_form_alter(&$form, &$form_state, $form_id) {
    switch ($form_id) {
    case "announcement_node_form":
      global $base_url;
      global $user;
      $form['notifications']['#type'] = array(
          '#type' => 'hidden',
      );
      $form['notifications']['notifications_content_disable'] = array(
        '#type' => 'hidden',
        '#value' => TRUE,
      );
      /*if (is_array($user->roles) && !in_array('administrator', $user->roles)) {
        unset($form['options']);
      }*/
      $referer_url = ltrim(str_replace($base_url, '', $_SERVER['HTTP_REFERER']), '/');
      // If using path alias then get original url to get nid.
      $internal_path = drupal_get_normal_path($referer_url);
      $url_array = explode('/', $internal_path);
      // Get nid From previous page as this will be the parent group.
      $gid = !empty($referer_url) && is_numeric($url_array[1]) ? $url_array[1] : '';
      // Set the og_group_ref to the parent nid.
      if (!empty($gid)) {
        $parent_node = node_load($gid);
        // Make sure the parent node is an organic group before assigning the
        // og_group_ref.
        if (isset($parent_node->group_group) && $parent_node->group_group['und'][0]['value'] == 1) {
          $form['og_group_ref']['und'][0]['default']['#default_value'][0] = $gid;
        }
      }
      $breadcrumb = drupal_get_breadcrumb();
      $group_link = l($parent_node->title, 'node/' . $gid);
      $breadcrumb[2] = $breadcrumb[1];
      $breadcrumb[1] = $group_link;
      unset($breadcrumb[2]);
      drupal_set_breadcrumb($breadcrumb);
      // Uncomment the line below to allow administrators to see the entire raw form.
      // if (is_array($user->roles) && !in_array('administrator', $user->roles)) {
      if (is_array($user->roles)) {
        // Set default values and hide them for non-admins.
        // Non-admins only need to create a subject (title) and body for
        // announcement.
        drupal_add_js(drupal_get_path('module', 'ogtabs') . '/js/announcement.js', array('scope' => 'footer'));
        //unset($form['field_announcement_subject']);
        $form['field_announcement_subject']['und'][0]['value']['#default_value'] = 'default value';
        $form['field_announcement_subject']['#prefix'] = '<div style="display:none;">';
        $form['field_announcement_subject']['#suffix'] = '</div>';
        $form['field_announcement_id']['#prefix'] = '<div style="display:none;">';
        $form['field_announcement_id']['#suffix'] = '</div>';
        $bb_course_id = db_query("SELECT field_blackboard_course_id_value
                                  FROM {field_data_field_blackboard_course_id}
                                  WHERE entity_id = :gid", array(':gid' => $gid))->fetchField();
        $field_announcement_id = !empty($bb_course_id) ? $bb_course_id : $gid . '_' . time();
        $form['field_announcement_id']['und'][0]['value']['#default_value'] = $field_announcement_id . ' - not created through blackboard';
        $form['field_sort_order']['#prefix'] = '<div style="display:none;">';
        $form['field_sort_order']['#suffix'] = '</div>';
        $form['field_sort_order']['und'][0]['value']['#default_value'] = 2;
        if (!empty($gid)) {
          $form['parent_gid'] = array(
            '#type' => 'hidden',
            '#value' => $gid,
          );
        }
        $form['#validate'][] = 'ogtabs_announcement_form_validate';
        $form['actions']['submit']['#submit'][] = 'ogtabs_announcement_form_submit';

      }
      break;

    case "blogs_node_form":
    case "blogs2_node_form":
      global $user;
      if (!isset($form['og_group_ref']['und'][0]['default']['#default_value'][0])) {
        $referrer_url = $_SERVER["HTTP_REFERER"];
        $referrer = explode('/', $referrer_url);
        $gid = $_GET['gid'];
        $form['og_group_ref']['und'][0]['default']['#default_value'][0] = $gid;
      }
      if (is_array($user->roles) && !in_array('administrator', $user->roles)) {
        $form['og_group_ref']['#access'] = FALSE;
      }
      break;
    case "wiki_node_form":
    case "poll_node_form":
    case "events_node_form":
    case "syllabi_node_form":
      global $base_url;
      global $user;
      if (is_array($user->roles) && !in_array('administrator', $user->roles)) {
        unset($form['options']);
      }
      $referer_url = ltrim(str_replace($base_url, '', $_SERVER['HTTP_REFERER']), '/');
      // If using path alias then get original url to get nid.
      $internal_path = drupal_get_normal_path($referer_url);
      $url_array = explode('/', $internal_path);
      // Get nid From previous page as this will be the parent group.
      $gid = !empty($referer_url) && is_numeric($url_array[1]) ? $url_array[1] : '';
      // Set the og_group_ref to the parent nid.
      if (!empty($gid)) {
        $parent_node = node_load($gid);
        // Make sure the parent node is an organic group before assigning the
        // og_group_ref.
        if (isset($parent_node->group_group) && $parent_node->group_group['und'][0]['value'] == 1) {
          $form['og_group_ref']['und'][0]['default']['#default_value'][0] = $gid;
        }
      }
      break;

    case "views_exposed_form":
      if (preg_match('/blogs-list/', $_GET['q']) || preg_match('/group-blogs/', $_GET['q']) || preg_match('/blogs2-list/', $_GET['q']) || preg_match('/group-blogs2/', $_GET['q']) ||
          preg_match('/wikis-list/', $_GET['q'])) {
        //drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>');
        $form['uid']['#autocomplete_path'] = 'user/autocomplete';
      };
      break;
  }
}

/**
 * OGTabs announcement form submit.
 */
function ogtabs_announcement_form_validate(&$form, &$form_state, $form_id) {
  $form_state['values']['field_announcement_subject']['und'][0]['value'] = $form_state['values']['title'];
}

/**
 * OGTabs announcement form submit.
 */
function ogtabs_announcement_form_submit(&$form, &$form_state, $form_id) {
  global $user;
  $gid = $form_state['values']['parent_gid'];
  if (!empty($gid)) {
    $course_title = db_query("SELECT title FROM {node} WHERE nid = :gid", array(':gid' => $gid))->fetchField();
    $announcement_title = $form_state['values']['field_announcement_subject']['und'][0]['value'];
    $subject = 'New Announcement Available in ' . $course_title . ' ' . $announcement_title;
    $body = 'New Announcement Available in <a href="http://bell.babson.edu/node/' . $gid . '">' . $course_title . '</a><br />  ' . $form_state['values']['body']['und'][0]['value'];
    $body .= 'To unsubscribe from this email click here:  <a href=' . $_SERVER[HTTP_HOST] .
    $mailfrom = 'drupal-admin@babson.edu';

    $members = db_query("SELECT DISTINCT(u.uid) AS uid, mail
                        FROM {users} u
                        JOIN {og_membership} ogm ON u.uid = ogm.etid AND ogm.entity_type = 'user' AND ogm.gid = :gid
                        WHERE u.status = 1
                        AND ogm.state = 1", array(':gid' => $gid));

    // Not implementing this to be dependent on og_notifications subscriptions at this time.
    // But leaving the code commented out for now in case we want to use it at some point.
    $subs = notifications_get_subscriptions(array('uid' => $user->uid, 'type' =>
          'group_content'), array('node:gid' => $gid));
    $subscribed = FALSE;
    if (isset($subs) && $subs !="") {
      foreach ($subs as $asub) {
        if ($asub->type == 'group_content') {
          $subscribed = TRUE;
        }
      }
    }
    if ($subscribed) {
      foreach ($members as $member) {
          $sid = db_query("SELECT ns.sid from notifications_subscription ns, notifications_subscription_fields nsf
           where ns.uid = :uid and nsf.sid = ns.sid and nsf.type = 'node:gid'", array('uid' => $member->uid))->fetchField();
          $body = 'New Announcement Available in <a href="http://bell.babson.edu/node/' . $gid . '">' . $course_title . '</a><br />  ' . $form_state['values']['body']['und'][0]['value'];
          $body .= 'To unsubscribe from announcements to this group click here:  <a href="http://' . $_SERVER[HTTP_HOST] . '/notifications/unsubscribe/' . $sid . '?destination=node/' . $gid . '"> Unsubscribe </a>';
          drupal_mail('ogtabs', 'announcement_email', $member->mail, user_preferred_language($user), array('body' => array($body), 'subject' => $subject), $mailfrom, TRUE);
      }
    }
  $messages = drupal_get_messages('status');

  drupal_set_message(t('The announcement has been created'));
    

  drupal_goto('node/' . $gid);
  }
}

/**
 * Implements form api to create content.
 */
function ogtabs_create_content_form($form) {
  global $user;
  if (module_exists('og_context')) {
    $current_group = og_context();
  }
  if (!isset($current_group['gid'])) {
    $curr_url = current_path();
    $path_array = explode('/', $curr_url);
    $gid = $path_array[1];
  }
  else {
    $gid = $current_group['gid'];
  }
  $form = array();
  $form['container'] = array();
  $type = db_query("SELECT type from {node} where nid = :gid", array('gid' => $gid))->fetchField();
  $options = variable_get('ogtabs_publishable', '');
  $select_options = array();

  foreach ($options as $item) {
    if (is_string($item)) {
      $singular_item = rtrim($item, 's');
      $select_options[$item] = t('Create @content_type', array('@content_type' => $singular_item));
    }
  }
  array_unshift($select_options, t('Select a type of content to create'));
  $form['container']['create_content_type'] = array(
    '#type' => 'select',
    '#title' => '',
    /*'#options' => array(
        0 => t('Select a type of content to create'),
        'announcement' => t('Create Announcement'),
        'blogs' => t('Create Blog Entry'),
        'poll' => t('Create Poll'),
        'wikis' => t('Create Wiki Entry'),
        'events' => t('Create Event'),
      ),*/
    '#options' => $select_options,
    '#default_value' => 0,
  );




  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['#attributes'] = array('id' => array('create-content-form'));
  return $form;
}

/**
 * Submit for form api.
 */
function ogtabs_create_content_form_submit($form, &$form_state) {
  if (module_exists('og_context')) {
    $current_group = og_context();
  }
  if (isset($current_group->gid)) {
    if (ogtabs_check_if_group_admin_or_ta($current_group->gid) || in_array('administrator', array_values($user->roles))) {
      if (count($keys) > 0) {
        if (module_exists('og_context')) {
          $current_group = og_context();
        }
        variable_set('ogtabs_non_group_admin_can_publish_' . $current_group['gid'], implode(",", $keys));
      }
      else {
        variable_del('ogtabs_non_group_admin_can_publish_' . $current_group['gid']);
      }
    }
  }
  if (!empty($form_state['values']['create_content_type'])) {
    if (module_exists('og_context')) {
      $current_group = og_context();
    }
    if (!isset($current_group['gid'])) {
      $referrer = $_SERVER['HTTP_REFERER'];
      $url_parse = parse_url($referrer);
      $url_path = $url_parse['path'];
      $path_array = explode('/', $url_path);
      $gid = $path_array[2];

    }
    else {
      $gid = $current_group['gid'];
    }
    $options = array('query' => array('gid' => $gid));
    drupal_goto('node/add/' . $form_state['values']['create_content_type'], $options);
  }
}

/**
 * Implements hook_mail().
 */
function ogtabs_mail($key, &$message, $params) {
  $message['subject'] = $params['subject'];
  $message['body'] = $params['body'];

  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'Drupal',
  );

  foreach ($headers as $key => $value) {
    $message['headers'][$key] = $value;
  }
}

/**
* Implementation of hook_views_default_views().
**/
function ogtabs_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'ogtabs') . '/views', '/.*\.view$/');
/*  watchdog('files', '<pre>' . print_r($files, TRUE) . '</pre>');*/
  foreach ($files as $absolute => $file) {
    require $absolute;
    if (isset($view)) {
      $views[$file->name] = $view;
    }   
  }
  return $views; 
}

function ogtabs_mail_alter(&$message) {
    $message['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';

}

/**
 * Access Callback seeing if the user is a Group Admin or TA
 * for this particular organic group.
 */
function ogtabs_check_if_group_admin_or_ta($gid){
    global $user;
    if (arg(0) == 'admin' && arg(1) == 'content' && arg(2) == 'enhanced_forum' && arg(3) == 'add' && arg(4) == 'forum') {
        // get gid from referer page if adding a forum.
        $referrer = $_SERVER['HTTP_REFERER'];
        $url_parse = parse_url($referrer);
        $url_path = $url_parse['path'];
        $path_array = explode('/', $url_path);
        $gid = $path_array[4];
    }
    if (((ogtabs_group_admin_check($gid) || ogtabs_group_ta_check($gid))) || in_array('administrator', $user->roles) || in_array('Super Production Admin', $user->roles) || $user->uid == 1) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function ogtabs_group_admin_check($gid, $uid = FALSE) {
    global $user;
    $thisref=$_SERVER['HTTP_REFERER'];
    if (preg_match('/enhanced_forum\/add\/forum/i', $thisref)) {
        return TRUE;
    }

    if (is_numeric($uid)) {
        $uid = $uid;
    }
    else {
        $uid = $user->uid;
    }
    $type = db_query("SELECT type FROM {node} WHERE nid = :group_nid", array(':group_nid' => $gid))->fetchField();
    // Get organic group administrator member rid.
    $admin_rid = db_query("SELECT rid FROM {og_role} WHERE name = :name AND group_bundle = :type AND gid =:gid", array(':name' => 'administrator member', ':type' => $type, ':gid' => $gid))->fetchField();
    if (!$admin_rid) {
        $admin_rid = db_query("SELECT rid FROM {og_role} WHERE name = :name AND group_bundle = :type", array(':name' => 'administrator member', ':type' => $type))->fetchField();
    }
    $group_admin = db_query("SELECT COUNT(*) FROM {og_users_roles} WHERE uid = :uid AND gid = :gid AND rid = :rid", array(':uid' => $uid, ':gid' => $gid, ':rid' => $admin_rid))->fetchField();

    if ($group_admin > 0) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function ogtabs_group_ta_check($gid) {
    global $user;
    $type = db_query("SELECT type FROM {node} WHERE nid = :group_nid", array(':group_nid' => $gid))->fetchField();
    // Get organic group TA member rid and ta_showuser rid.
    if ($type == 'group') {
        $ta_rid = db_query("SELECT rid FROM {og_role} WHERE group_bundle = 'group' AND name = :name AND gid = :gid", array(':name' => 'ta', ':gid' => $gid))->fetchField();
        if (!$ta_rid) {
            $ta_rid = db_query("SELECT rid FROM {og_role} WHERE name = :name AND group_bundle = :type", array(':name' => 'ta', ':type' => 'group'))->fetchField();
        }
        $group_ta = db_query("SELECT COUNT(*) FROM {og_users_roles} WHERE uid = :uid AND gid = :gid AND rid = :rid", array(':uid' => $user->uid, ':gid' => $gid, ':rid' => $ta_rid))->fetchField();
        $ta_showuser_rid = db_query("SELECT rid FROM {og_role} WHERE group_bundle = 'group' AND name = :name AND gid = :gid", array(':name' => 'ta_showuser', ':gid' => $gid))->fetchField();
        if (!$ta_showuser_rid) {
            $ta_showuser_rid = db_query("SELECT rid FROM {og_role} WHERE name = :name AND group_bundle = :type", array(':name' => 'ta_showuser', ':type' => 'group'))->fetchField();
        }
        $group_ta_showuser = db_query("SELECT COUNT(*) FROM {og_users_roles} WHERE uid = :uid AND gid = :gid AND rid = :rid", array(':uid' => $user->uid, ':gid' => $gid, ':rid' => $ta_showuser_rid))->fetchField();
    }
    else {
        return FALSE;
    }

    if ($group_ta > 0 || $group_ta_showuser > 0) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}