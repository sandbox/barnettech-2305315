(function($) {
    $(document).ready(function() {
      // Hide draggable handle icon.
      $('#field-announcement-subject-add-more-wrapper tr:nth-child(1)').find('.handle').hide();
      // Hide 2nd and 3rd title textareas.
      $('#field-announcement-subject-add-more-wrapper tr:nth-child(2)').hide();
      $('#field-announcement-subject-add-more-wrapper tr:nth-child(3)').hide();
      // Hide weight toggle link.
      $('#field-announcement-subject-add-more-wrapper').find('.tabledrag-toggle-weight-wrapper').hide();
    });          
}(jQuery));

