<?php

/**
 * @file
 * Pages for syllabus.
 */

/**
 * Builds out syllabi list.
 */
function ogtabs_syllabi_list() {
  $output = theme('ogtabs_syllabi_list');
  return $output;
}

/**
 * Theme function that builds out syllabi list.
 */
function theme_ogtabs_syllabi_list() {
  global $base_url;
  include 'ogtabs_parent_form.inc';
  drupal_add_js(drupal_get_path('module', 'ogtabs') . '/js/syllabus.js', array('scope' => 'footer'));
  drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/syllabus.css');
  $content = '<div class="title"> Syllabus </div>';
  $gid = arg(1);
  $syllabi = db_query("SELECT n.nid, n.title, fdb.body_value from {node} n, {field_data_body} fdb, {og_membership} om where fdb.entity_id = n.nid and om.etid = n.nid and n.type = 'syllabi' and om.gid = :gid",
    array('gid' => $gid));
  $syllabi_count = db_query("SELECT count(n.nid) from {node} n, {field_data_body} fdb, {og_membership} om where fdb.entity_id = n.nid and om.etid = n.nid and n.type = 'syllabi' and om.gid = :gid",
        array('gid' => $gid))->fetchField();
  $result = ogtabs_parent_get_data();

  $content = "";
  foreach ($result as $item) {
    $indent = theme('indentation', array('size' => $item->depth));
    // I put the below in a tpl file instead.
    /*if (ogtabs_check_if_group_admin_or_ta($gid)) {
      $content .= "<span class='syllabus-edit'><a href='" . $base_url . "/node/" . $item->id . "/edit'>Edit</a></span>";
    }*/
    unset($item->depth);
    if (is_numeric($item->id)) {
      $view = node_view(node_load($item->id), 'full');
    }
    $ogtabs_entry = drupal_render($view);
    $content .= $indent . "<span class='syllabus-entry'>" . $ogtabs_entry . "</span>";
    $content .= "<span class='spacer'>&nbsp;</span>";
  }
  return $content;
}

/**
 * Function that builds out the blog list.
 */
function ogtabs_tab_blogs_list() {
  $output = theme('ogtabs_tab_blogs_list');
  return $output;
}

/**
 * Theme function that builds out blog list.
 */
function theme_ogtabs_tab_blogs_list() {
  global $user;
  drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
  $content = '<div class="ogtabs-content-wrapper">';
  $content .= '<div class="title page-header-no-collapse"> Blogs </div>';
  $temp_filter_form =  drupal_get_form('ogtabs_blog_form');
  $filter_form = drupal_render($temp_filter_form);
  $content .= $filter_form;
  $gid = arg(1);
  if (isset($_SESSION['blog_filter']) && strlen($_SESSION['blog_filter']) > 0) {
    $filter = $_SESSION['blog_filter'];
    unset($_SESSION['blog_filter']);
    $filter_uid = db_query('SELECT uid from {users} where name = :filter', array('filter' => $filter))->fetchField();
  }
  if (ogtabs_check_if_group_admin_or_ta($gid)) {
    if (isset($filter)) {
      $blogs = db_query("SELECT DISTINCT(n.nid), n.uid, n.title, n.created from {node} n, {og_membership} om, {field_data_field_blog_visibility} v where om.etid = n.nid and v.entity_id = n.nid and n.type = 'blogs' and n.uid = :filter and om.gid = :gid and (v.field_blog_visibility_value in ('instructor', 'whole_class') or n.uid = :uid) order by n.created desc", array('gid' => $gid, 'uid' => $user->uid, 'filter' => $filter_uid));
    }
    else {
      $blogs = db_query("SELECT DISTINCT(n.nid), n.uid, n.title, n.created from {node} n, {og_membership} om, {field_data_field_blog_visibility} v where om.etid = n.nid and v.entity_id = n.nid and n.type = 'blogs' and om.gid = :gid and (v.field_blog_visibility_value in ('instructor', 'whole_class') or n.uid = :uid) order by n.created desc", array('gid' => $gid, 'uid' => $user->uid));
    }
  }
  else {
    if (isset($filter)) {
      $blogs = db_query("SELECT DISTINCT(n.nid), n.title, n.uid, n.created from {node} n, {og_membership} om, {field_data_field_blog_visibility} v where om.etid = n.nid and v.entity_id = n.nid and n.type = 'blogs' and n.uid = :filter and om.gid = :gid and (v.field_blog_visibility_value in ('whole_class') or n.uid = :uid) order by n.created desc", array('gid' => $gid, 'uid' => $user->uid, 'filter' => $filter_uid));

    }
    else {
      // The query for all students.
      $blogs = db_query("SELECT DISTINCT(n.nid), n.title, n.created, n.uid from {node} n, {og_membership} om, {field_data_field_blog_visibility} v where om.etid = n.nid and v.entity_id = n.nid and n.type = 'blogs' and om.gid = :gid and (v.field_blog_visibility_value in ('whole_class') or n.uid = :uid) order by n.created desc", array('gid' => $gid, 'uid' => $user->uid));
    }
  }
  foreach ($blogs as $blog) {
    $author = user_load($blog->uid);
    if (ogtabs_check_if_group_admin_or_ta($gid) || $blog->uid == $user->uid) {
      $edit_link = l(t('edit'), 'node/' . $blog->nid . '/edit');
    }
    else {
      $edit_link = '';
    }

    $theme_name = theme('username', array('account' => $author));
    $content .= '<div class="ogtabs-link-wrapper">';
    $content .= $theme_name;
    $content .= '<div class="ogtabs-content-row">' . l($blog->title, 'node/' . $gid . '/blogs-list/blog/' . $blog->nid) . '</div>';
    //$content .= '<div class="ogtabs-content-row">' . l($blog->title, 'node/' . $blog->nid) . '</div>';
    $content .= '<div class="ogtabs-content-row"> ' . format_date($blog->created, 'medium', 'F j, Y, g:i a') . '</div>';
    $content .= '<div class="ogtabs-edit-button" >   ' . $edit_link . '</div><div></div>';
    $content .= '</div><!--end of ogtabs-link-wrapper div -->';
  }
  $content .= '</div><!--end of ogtabs-content-wrapper div -->';
  return $content;
}

/**
 * Builds out blog list.
 */
function ogtabs_tab_blogs2_list() {
  $output = theme('ogtabs_tab_blogs2_list');
  return $output;
}

/**
 * Theme function that builds out blog list.
 */
function theme_ogtabs_tab_blogs2_list() {
    drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
    $content = '<div class="ogtabs-content-wrapper">';
    $content .= '<div class="title page-header-no-collapse"> Blogs </div>';
    $gid = arg(1);
    $blogs2 = db_query("SELECT n.nid, n.title from {node} n, {og_membership} om where om.etid = n.nid and n.type = 'blogs2' and om.gid = :gid",
        array('gid' => $gid));
    foreach ($blogs2 as $blog2) {
        if (ogtabs_check_if_group_admin_or_ta($gid)) {
            $edit_link = l(t('edit'), 'node/' . $blog2->nid . '/edit');
        }
        else {
            $edit_link = '';
        }
        $content .= '<div class="ogtabs-link-wrapper">';
        $content .= '<div class="ogtabs-content-row">' . l($blog2->title, 'node/' . $gid . '/blogs2-list/blogs2/' . $blog2->nid) . '</div>';
        // $content .= '<div class="ogtabs-content-row">' . l($blog2->title, 'node/' . $blog2->nid) . '</div>';
        $content .= '<div class="ogtabs-edit-button" >' . $edit_link . '</div><div></div>';
        $content .= '</div><!--end of ogtabs-link-wrapper div -->';
    }
    $content .= '</div><!--end of ogtabs-content-wrapper div -->';
    return $content;
}

/**
 * Builds out events list.
 */
function ogtabs_tab_events_list() {
  $output = theme('ogtabs_tab_events_list');
  return $output;
}

/**
 * Theme function that builds out events list.
 */
function theme_ogtabs_tab_events_list() {
  drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
  $content = '<div class="ogtabs-content-wrapper">';
  $content .= '<div class="title page-header-no-collapse"> Events </div>';
  $gid = arg(1);
  $events = db_query("SELECT n.nid, n.title from {node} n, {og_membership} om where om.etid = n.nid and n.type = 'events' and om.gid = :gid",
    array('gid' => $gid));
  foreach ($events as $event) {
    if (ogtabs_check_if_group_admin_or_ta($gid)) {
      $edit_link = l(t('edit'), 'node/' . $event->nid . '/edit');
    }
    else {
      $edit_link = '';
    }
    $content .= '<div class="ogtabs-link-wrapper">';
    $content .= '<div class="ogtabs-content-row">' . l($event->title, 'node/' . $gid . '/event-list/blog/' . $event->nid) . '</div>';
    //$content .= '<div class="ogtabs-content-row">' . l($event->title, 'node/' . $event->nid) . '</div>';
    $content .= '<div class="ogtabs-edit-button" >' . $edit_link . '</div><div></div>';
    $content .= '</div><!--end of ogtabs-link-wrapper div -->';
  }
  $content .= '</div><!--end of ogtabs-content-wrapper div -->';
  return $content;
}

/**
 * Builds out wikis list.
 */
function ogtabs_tab_wiki_list() {
  $output = theme('ogtabs_tab_wiki_list');
  return $output;
}

/**
 * Theme function that builds out wikis list.
 */
function theme_ogtabs_tab_wiki_list() {
    drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
    $content = '<div class="ogtabs-content-wrapper">';
    $content .= '<div class="title page-header-no-collapse"> Wikis </div>';
    $gid = arg(1);
    $wikis = db_query("SELECT n.nid, n.title from {node} n, {og_membership} om where om.etid = n.nid and n.type = 'wiki' and om.gid = :gid",
        array('gid' => $gid));
    foreach ($wikis as $wiki) {
        if (ogtabs_check_if_group_admin_or_ta($gid)) {
            $edit_link = l(t('edit'), 'node/' . $wiki->nid . '/edit');
        }
        else {
            $edit_link = '';
        }
        $content .= '<div class="ogtabs-link-wrapper">';
        $content .= '<div class="ogtabs-content-row">' . l($wiki->title, 'node/' . $gid . '/wiki-list/wiki/' . $wiki->nid) . '</div>';
        // $content .= '<div class="ogtabs-content-row">' . l($wiki->title, 'node/' . $wiki->nid) . '</div>';
        $content .= '<div class="ogtabs-edit-button" >' . $edit_link . '</div><div></div>';
        $content .= '</div><!--end of ogtabs-link-wrapper div -->';
    }
    $content .= '</div><!--end of ogtabs-content-wrapper div -->';
    return $content;

}

/**
 * Builds out poll list.
 */
function ogtabs_tab_poll_list() {
  $output = theme('ogtabs_tab_poll_list');
  return $output;
}

/**
 * Theme function that builds out poll list.
 */
function theme_ogtabs_tab_poll_list() {
  drupal_add_js(drupal_get_path('module', 'babson_groups') . '/js/polls.js', array('scope' => 'footer'));
  drupal_add_css(drupal_get_path('module', 'babson_groups') . '/css/polls.css');
  drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
  $content = '<div class="ogtabs-content-wrapper">';
  $content .= '<div class="title page-header-no-collapse"> POLLS </div>';
  $gid = arg(1);
  $polls = db_query("SELECT n.nid, n.title from {node} n, {og_membership} om where om.etid = n.nid and n.type = 'poll' and om.gid = :gid",
    array('gid' => $gid));
  foreach ($polls as $poll) {
    if (ogtabs_check_if_group_admin_or_ta($gid)) {
      $edit_link = l(t('edit'), 'node/' . $poll->nid . '/edit');
    }
    else {
      $edit_link = '';
    }
    $content .= '<div class="ogtabs-link-wrapper">';
    $content .= '<div class="ogtabs-content-row">' . l($poll->title, 'node/' . $gid . '/poll-list/poll/' . $poll->nid) . '</div>';
    //$content .= '<div class="ogtabs-content-row">' . l($poll->title, 'node/' . $gid . '/poll-list/poll/' . $poll->nid) . '</div>';
    $content .= '<div class="ogtabs-edit-button" >' . $edit_link . '</div><div></div>';
    $content .= '</div><!--end of ogtabs-link-wrapper div -->';
  }
  $content .= '</div><!--end of ogtabs-content-wrapper div -->';
  return $content;
}

/**
 * Builds out calendar page.
 */
function ogtabs_calendar_page() {
  drupal_add_css(drupal_get_path('module', 'ogtabs') . '/css/ogtabs.css');
  $gid = arg(1);
  $block = views_embed_view('group_full_calendars', 'default', $gid);
  return $block;
}